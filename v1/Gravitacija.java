import java.util.*;
import java.lang.Math;

class Gravitacija {

    
    
    public static void main (String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        
        int visina = sc.nextInt();
        System.out.println("OIS je zakon!");
        
        double grav = gravitacijskiPospesek(visina);
        
        izpis(visina, grav);
    }
    
    
    
    public static double gravitacijskiPospesek(int v){
        double C = ( 6.674 * Math.pow(10, -11) ) ;
        double  M = ( 5.972 * Math.pow(10, 24) ) ;
        double  r = ( 6.371 * Math.pow(10, 6) );
        
        return (C * M) / ( Math.pow(r+v, 2) );
        
        
    }
    
    


   
   public static void izpis(int visina, double gravitacija){
      
      System.out.println(visina);
      System.out.println(gravitacija);      
     
   }
   
}